from django.db import models

# Create your models here.
class Mecanico(models.Model):
    id=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=150)
    especialidad=models.CharField(max_length=150)
    telefono=models.CharField(max_length=10)
    correo=models.EmailField()
    foto=models.FileField(upload_to='mecanico', null=True, blank=True)


    def _str_(self):
        fila="{0}: {1} {2} - {3}"
        return fila.format(self.nombre, self.especialidad, self.telefono, self.correo, self.foto)
