from django.shortcuts import render, redirect
from .models import Mecanico
from django.contrib import messages
# Create your views here.
def plantilla(request):
    return render(request,'cuerpo.html')

def listadoMecanico(request):
    mecanicosBdd = Mecanico.objects.all()
    return render(request, 'mecanico/mecanico.html', {'mecanicos': mecanicosBdd})
